import React, { Component } from 'react'

// Modules
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { ConectDatabase } from './utils/databaseFunctions'

// Routes
import routes from './routes'

// Components
import Screen  from './components/Screen'

// conectando ao banco
ConectDatabase()

// Construindo as rotas com base no arquivo routes.js e templates
let routeToRender = {}
routes.forEach(route => {
	routeToRender[route['route']] = {
		screen: 
			class ScreenRoute extends Component {
				constructor(props){
					super(props)
				}

				render(){
					return <Screen screenParams={route['screen']} navigation={this.props.navigation} />
				}
			},
		navigationOptions: { header: null, animationEnabled: false }
	}
})

const AppNavigator = createStackNavigator(routeToRender)

const App = createAppContainer(AppNavigator)

export default App
