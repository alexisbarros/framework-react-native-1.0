// Templates
import splash from './templates/custom/splash'
import login from './templates/custom/login'
import signup from './templates/custom/signup'
import forgotPass from './templates/custom/forgot_pass'
import template_list from './templates/lists/template_list'
import template_form from './templates/forms/template_form'

// Configurações gerais do app
const configs = {
    appName: 'My Framework',
    shortDescription: 'Build awesome apps',
    colors: {
        primary: '#9c5b01',
        secondary: '#f8db27',
        tertiary: null
    },
    headerMenuLinks: [
        {route: 'List', label: 'Lista', icon: 'list-alt'},
        {route: 'Form', label: 'Form', icon: 'inbox'},
    ],
    footerMenuLinks: [
        {route: 'Home', label: 'Início', icon: 'home'},
        {route: 'List', label: 'Lista', icon: 'list-alt'},
        {route: 'Form', label: 'Form', icon: 'inbox'},
    ],
    // Usar os localUsers apenas se não utilizar o login no firebase
    // localUsers: [
    //     { email: 'teste1', password: '123', id: '00'},
    //     { email: 'teste2', password: '321', id: '01'},
    // ]
}

// Rotas
// Informar em cada rota o nome da rota, o(s) template(s) e se terá Header e Footer
const routes = [
    // Splash screen
    {route: 'Splash', screen: {templates: [splash], configs, menu: {header: false, footer: false}, accessOnlyLoggedUser: true, splash: true } },
    
    {route: 'Home', screen: {templates: [], configs, menu: {header: true, footer: true}, accessOnlyLoggedUser: true } }, // Rota da página inicial sem template
    {route: 'List', screen: {templates: [template_list], configs, menu: {header: true, footer: true}, accessOnlyLoggedUser: true } },
    {route: 'Form', screen: {templates: [template_form], configs, menu: {header: true, footer: true}, accessOnlyLoggedUser: true } },
    
    // Componentes de autenticação prontos 
    {route: 'Login', screen: {templates: [login], configs, menu: {header: false, footer: false} } }, // Componente Login pronto
    {route: 'Signup', screen: {templates: [signup], configs, menu: {header: false, footer: false} } }, // Componente Signup pronto
    {route: 'ForgotPass', screen: {templates: [forgotPass], configs, menu: {header: false, footer: false} } }, // Componente Signup pronto
]

export default routes