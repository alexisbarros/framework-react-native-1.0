/* Funções comuns que se repetem contantemente */

// Cria uma mascara que pode ser usada em campos de formulário (como tel, cpf e cnpj por exemplo)
// Parametros: mask (ex.: xxx.xxx.xxx-xx), value a ser aplicado a mask, maxSize (qtd de dígitos)
// Retorno: value com a mascara aplicada
function CreateMask(mask, value, maxSize) {
    if(value){
        if(value.length <= maxSize){
            // Criar array com os simbolos e posições
            let array = []
            let arrayMask = mask.split('')
            let count = 0
            arrayMask.forEach(element => {
                if(element !== 'x'){
                    array.push({ symbol: element, position: count })
                }
                count += 1
            })
            array.forEach(x => {
                if(value.length > x.position && value[x.position] !== x.symbol){
                    let valueArray = value.split('')
                    valueArray.splice(x.position, 0, x.symbol)
                    value = valueArray.join('')
                }
            })
            return value
        } else {
            return value.substring(0, maxSize)
        }
    }
}

// Função para formatar os campos de data no padrão pt-BR
// Parametros: date
// Retorno: string no formato DD/MM/YYYY
function FormatDate(date) {
    /* eslint no-confusing-arrow: 0 */
    const pad = n => n < 10 ? `0${n}` : n
    const dateStr = `${pad(date.getDate())}/${pad(date.getMonth() + 1)}/${date.getFullYear()}`
    return `${dateStr}`
}

export {
    CreateMask,
    FormatDate
}