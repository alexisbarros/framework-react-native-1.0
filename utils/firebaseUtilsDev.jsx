const firebaseConfigs = {
    apiKey: "AIzaSyDmtwLetwcZQbaF52GPFbrh9kpbmUwKxG0",
    authDomain: "framework-react-native.firebaseapp.com",
    databaseURL: "https://framework-react-native.firebaseio.com",
    projectId: "framework-react-native",
    storageBucket: "framework-react-native.appspot.com",
    messagingSenderId: "264991407384",
    appId: "1:264991407384:web:385d3a8502d7ad8245b26d",
    measurementId: "G-P9PHQ36SLG"
}

const endpoint = 'https://us-central1-framework-react-native.cloudfunctions.net'

export {
    firebaseConfigs,
    endpoint
}