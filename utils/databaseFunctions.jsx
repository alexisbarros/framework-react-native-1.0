/* Funções para manipulação do banco */

// Pacotes necessários
import firebase from 'firebase'
import { firebaseConfigs, endpoint } from './firebaseUtilsDev'
import { FormatDate } from './commonFunctions'

// Conexão ao banco
function ConectDatabase(props){
    firebase.initializeApp(firebaseConfigs)
}

// Cadastro de usuário no banco
// Parametros: email, password, appName
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
function SignIn(props){
    return firebase.auth().createUserWithEmailAndPassword(props.email, props.password)
    .then(() => {
        firebase.auth().onAuthStateChanged(user => localStorage.setItem(props.appName, JSON.stringify({email: user.email, id: user.uid}) ) )
        return({
            code: 1,
            msg: 'Usuário cadastrado com sucesso'
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao cadastrar usuário, tente novamente',
            adminMsg: err.message
        })
    })
}

// Resetar a senha
// Parametros: email
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
function ResetPass(props){
    return firebase.auth().sendPasswordResetEmail(props.email)
    .then(() => {
        return({
            code: 1,
            msg: 'E-mail enviado, verifique sua caixa de entrada.'
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao enviar e-mail, tente novamente.',
            adminMsg: err.message
        })
    })
}

// Login de usuário
// Parametros: email, password, appName
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
function LogIn(props){
    return firebase.auth().signInWithEmailAndPassword(props.email, props.password)
    .then(() => {
        firebase.auth().onAuthStateChanged(user => localStorage.setItem(props.appName, JSON.stringify({email: user.email, id: user.uid}) ) )
        return({
            code: 1,
            msg: 'Usuário logado com sucesso'
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao logar usuário, tente novamente',
            adminMsg: err.message
        })
    })
}

// Login local de usuário
// Parametros: email, password, appName, localUsers
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
function LocalLogIn(props){
    let loginSuccess = false
    let userId = null
    props.localUsers.forEach(user => {
        if(user.email === props.email && user.password === props.password){
            loginSuccess = true
            userId = user.id
        }
    })
    if(loginSuccess){
        localStorage.setItem(props.appName, JSON.stringify({email: props.email, id: userId}) )
        return({
            code: 1,
            msg: 'Usuário logado com sucesso'
        })
    } else {
        return({
            code: 0,
            msg: 'Erro ao logar usuário, tente novamente',
            adminMsg: 'Erro ao logar usuário, tente novamente'
        })
    }
}

// Logout de usuário
// Parametros: appName
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
function LogOut(props){
    return firebase.auth().signOut()
    .then(() => {
        localStorage.removeItem(props.appName)
        return({
            code: 1,
            msg: 'Usuário deslogado com sucesso'
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao deslogar usuário, tente novamente',
            adminMsg: err.message
        })
    })
}

// Checar se usuário está logado no sistema
// Parametros: null
// Retorno: true ou false
async function GetUserInfo(props){
    let userFromDb = await firebase.auth().currentUser
    return userFromDb
}

// Trazer email do usuário logado
// Parametros: null
// Retorno: email
function GetUserEmail(props){
    return firebase.auth().onAuthStateChanged(user => {
        return user.email
    })
}

// Registar dado no banco
// Parametros: ref, data, appName
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg, id do objeto criado
async function Create(props){
    if(navigator.onLine){
        let newDataRef = firebase.database().ref(props.ref).push()
        let user = JSON.parse(localStorage.getItem(`${props.appName}`))
        return newDataRef.set({...props.data, id: newDataRef.key, userId: user.id, createdAt: FormatDate(new Date()), updatedAt: FormatDate(new Date()) })
        .then(() => {
            return({
                code: 1,
                msg: 'Registro realizado com sucesso',
                id: newDataRef.key
            })
        })
        .catch(err => {
            return({
                code: 0,
                msg: 'Erro ao realizar o registro, tente novamente',
                adminMsg: err.message
            })
        })
    } else {
        let objsToPush = JSON.parse(localStorage.getItem(`${props.appName}_objsToPush`)) || []
        objsToPush.push({ref: props.ref, data: props.data})
        localStorage.setItem(`${props.appName}_objsToPush`, JSON.stringify(objsToPush) )
        return({
            code: 1,
            msg: 'Registro realizado localmente, aguarde uma conexão para enviar para o banco'
        })
    }
}

// Fazer upload de documentos no banco
// Parametros: ref, data
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
function UploadFile(props){
    return firebase.storage().ref().child(props.ref).put(props.data)
    .then(() => {
        return({
            code: 1,
            msg: 'Upload realizado com sucesso'
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao realizar o upload, tente novamente',
            adminMsg: err.message
        })
    })
}

// Trazer URL para download de documentos no banco
// Parametros: ref
// Retorno: code: 1 para sucesso e 0 para erro, msg, url
function GetUrlFile(props){
    return firebase.storage().ref().child(props.ref).getDownloadURL()
    .then(url => {
        return({
            code: 1,
            msg: 'Url recebida com sucesso',
            url: url
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao tentar buscar a Url, tente novamente',
        })
    })
}

// Buscar dado no banco
// Parametros: ref, fieldToSearch, operator ('equal', 'like'), value
// Retorno: Array de resultado
async function Read(props){
    let response = await fetch(endpoint + '/read', {
        method: "POST",
        body: JSON.stringify(props)
    })
    let result = await response.json()
    return result
}

// Atualziar dado no banco
// Parametros: ref, data, appName, localIndex, localRef
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
async function Update(props){
    if(navigator.onLine){
        let user = JSON.parse(localStorage.getItem(`${props.appName}`))
        return firebase.database().ref(props.ref).update({...props.data, updatedAt: FormatDate(new Date()), userIdUpdated: user.id})
        .then(() => {
            return({
                code: 1,
                msg: 'Atualização realizada com sucesso'
            })
        })
        .catch(err => {
            return({
                code: 0,
                msg: 'Erro ao realizar a atualização, tente novamente',
                adminMsg: err.message
            })
        })
    } else {
        let objsToPush = JSON.parse(localStorage.getItem(`${props.appName}_objsToPush`))
        objsToPush[props.localIndex] = {ref: props.localRef, data: props.data}
        localStorage.setItem(`${props.appName}_objsToPush`, JSON.stringify(objsToPush) )
        return({
            code: 1,
            msg: 'Registro atualizado localmente, aguarde uma conexão para enviar para o banco'
        })
    }
}

// Deletar dado no banco
// Parametros: ref
// Retorno: code: 1 para sucesso e 0 para erro, msg, adminMsg
async function Delete(props){
    return firebase.database().ref(props.ref).remove()
    .then(() => {
        return({
            code: 1,
            msg: 'Exclusão realizada com sucesso'
        })
    })
    .catch(err => {
        return({
            code: 0,
            msg: 'Erro ao realizar a exclusão, tente novamente',
            adminMsg: err.message
        })
    })
}

// Sincronizar: Salvar dados locais no banco
// Parametros: appName
// Retorno: code: 1 para sucesso e 0 para erro, msg
async function Sinc(props){
    let localObjToPush = JSON.parse(localStorage.getItem(`${props.appName}_objsToPush`)) || []
    localObjToPush.forEach((obj, i) => {
        Create({
            ref: obj.ref,
            data: obj.data,
            appName: props.appName
        })
        .then(() => {
            if(i === (localObjToPush.length - 1)){
                localStorage.removeItem(`${props.appName}_objsToPush`)
                props.refreshScreen()
            }
        })
    })
}

export {
    ConectDatabase,
    SignIn,
    ResetPass,
    LogIn,
    LocalLogIn,
    LogOut,
    GetUserInfo,
    GetUserEmail,
    Create,
    UploadFile,
    GetUrlFile,
    Read,
    Update,
    Delete,
    Sinc
}