const firebaseConfigs = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: "",
    measurementId: ""
}

const endpoint = 'https://us-central1-framework-react-native.cloudfunctions.net'

export {
    firebaseConfigs,
    endpoint
}