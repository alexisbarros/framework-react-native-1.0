import React, { Component } from 'react'

// Modules
import { AppLoading } from 'expo'
// import * as Font from 'expo-font' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { View, Text, StyleSheet } from 'react-native'
import { Icon } from 'native-base'
// import { Provider, Flex, Button, SearchBar, ActivityIndicator, Popover, Modal } from '@ant-design/react-native' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { Flex, Button, SearchBar, ActivityIndicator, Popover, Modal } from 'antd-mobile' // PWA Projects: Utilize para projetos web (PWA)
import { Read, Sinc, Delete } from '../utils/databaseFunctions'

class ListComponent extends Component {

    constructor(props){
        super(props)
        this.state = {
            isReady: false,
            loadingList: true,
            name: this.props.name,
            fullList: [],
            list: [],
            info: this.props.template,
            itemOptionMenu: [],
            pageNumber: 1,
            count: 0,
            showMoreResultInfo: false,
            searchValue: ""
        }
    }

    // Native Projects: Utilize para projetos nativos (IOS ou Android)
    // async componentDidMount() {
    //     await Font.loadAsync(
    //         'antoutline',
    //         // eslint-disable-next-line
    //         require('@ant-design/icons-react-native/fonts/antoutline.ttf')
    //     )
    
    //     await Font.loadAsync(
    //         'antfill',
    //         // eslint-disable-next-line
    //         require('@ant-design/icons-react-native/fonts/antfill.ttf')
    //     )

    //     this.setState({ isReady: true })

    //     this.getList(1)
    // }

    // PWA Projects: Utilize para projetos web (PWA)
    componentDidMount = async () => {
        this.setState({ isReady: true })
        this.getList(1)
    }
    
    getList = (pageNumber, fieldToSearch = null, operator = null, value = null) => {
        this.setState({ loadingList: true, showMoreResultInfo: false })

        Read({ ref: this.state.name, pageNumber, fieldToSearch, operator, value })
        .then(res => {
            let list = res.resultArray
            let count = res.countResult
            // Todo: trabalhar em um módulo de permissão
            // list = list.filter(item => {
            //     let userId = JSON.parse(localStorage.getItem(`${this.props.appName}`)).id
            //     if(!this.props.filterByUsers) return item
            //     else if(this.props.filterByUsers.includes(userId) || (this.props.filterByUsers.includes('owner') && item.userId === userId)) return item
            //     else return null
            // })

            // Ocultar menu dos itens
            let itemOptionMenu = []
            itemOptionMenu = list.map(() => false)

            this.setState({ 
                list, 
                fullList: list, 
                loadingList: false, 
                itemOptionMenu, 
                pageNumber: this.state.pageNumber + 1, 
                count,
                showMoreResultInfo: (list.length < count ? true : false)
            })
        })
    }

    refreshScreen = () => {
        this.componentDidMount()
    }

    filterList = (value) => {
        // let localFilteredList = this.state.fullList.filter(v => {
        //     if(!value) return v

        //     let found = false
        //     this.state.info.searchOptions.fieldsToSearch.forEach(field => {
        //         if(v[field].toString().toLowerCase().includes(value.toString().toLowerCase())) found = true
        //     })

        //     if(found) return v
        // })
        // this.setState({ list: localFilteredList })
    }

    render(){

        if (!this.state.isReady) {
            return <AppLoading />;
        }

        let list = this.state.list.map((item, i) => {
            return(
                <View key={item.id} style={styles.card}>
                    <Popover mask
                        overlayClassName="fortest"
                        overlayStyle={{ color: 'currentColor' }}
                        visible={this.state.itemOptionMenu[i]}
                        overlay={[
                            (<Popover.Item key="0" value="delete" icon={<Icon type="FontAwesome" name='trash' style={{ fontSize: 'smaller', color: 'gray' }} />} data-seed="logId"><Text style={{ color: 'gray' }}>Deletar</Text></Popover.Item>),
                        ]}
                        align={{
                            overflow: { adjustY: 0, adjustX: 0 },
                            offset: [7, 5],
                        }}
                        onVisibleChange={e => {
                            let itemOptionMenu = this.state.itemOptionMenu
                            itemOptionMenu[i] = e
                            this.setState({ itemOptionMenu })
                        }}
                        onSelect={e => {
                            let itemOptionMenu = this.state.itemOptionMenu
                            itemOptionMenu[i] = false
                            this.setState({ itemOptionMenu })
                            Modal.alert(
                                <Text style={{ fontSize: 18 }}>Aviso!</Text>, <Text style={{ color: '#8c8c8c' }}>Tem certeza que deseja excluir esse registro?</Text>, 
                                [
                                    { text: <Text>Sim</Text>, style: 'default', onPress: () => {
                                        Delete({ref: `${this.state.name}/${item.id}`})
                                        .then(() => { this.componentDidMount() })
                                    }},
                                    { text: <Text>Não</Text>, style: 'default'},
                                ]
                            )
                        }}
                    >
                        <Text style={{ textAlign: 'right' }}>
                            <Icon type="FontAwesome" name='ellipsis-h' style={{ fontSize: 'smaller', color: 'gray' }} />
                        </Text>
                    </Popover>
                    <View
                        onClick={() => this.props.navigation.push(this.state.info['routeToUpdateData'], { objectToEdit: item })}
                        onPress={() => this.props.navigation.push(this.state.info['routeToUpdateData'], { objectToEdit: item })}
                    >
                        <Text style={{ fontSize: 16 }}>{item[this.state.info['title'] || null]}</Text>
                        <Text style={{ fontSize: 12, marginTop: 8 }}>{item[this.state.info['subtitle'] || null]}</Text>
                        <Text style={{ fontSize: 10, marginTop: 8 }}>{item[this.state.info['extra'] || null]}</Text>
                    </View>
                </View>
            )
        })

        let localList = (JSON.parse(localStorage.getItem(`${this.props.appName}_objsToPush`)) || []).map((item, i) => {
            if(item.ref === this.state.name)
                return(
                    <View key={i} style={styles.card}
                        onClick={() => this.props.navigation.push(this.state.info['routeToUpdateData'], { objectToEdit: {...item.data, localIndex: i} })}
                        onPress={() => this.props.navigation.push(this.state.info['routeToUpdateData'], { objectToEdit: {...item.data, localIndex: i} })}
                    >
                        <Icon type="FontAwesome" name='info' style={{fontSize: 20, color: 'orange'}} /> <Text style={{ fontSize: 16 }}>{item.data[this.state.info['title'] || null]}</Text>
                        <Text style={{ fontSize: 12, marginTop: 8 }}>{item.data[this.state.info['subtitle'] || null]}</Text>
                        <Text style={{ fontSize: 10, marginTop: 8 }}>{item.data[this.state.info['extra'] || null]}</Text>
                    </View>
                )
            return null
        })

        return(
            // Native Projects: Utilize a tag Provider para projetos nativos (IOS ou Android), para web (PWA) utilize a tag View
            <View>
                <Flex style={{ margin: 10, backgroundColor: 'rgb(245, 245, 245)' }}>
                    <Flex.Item style={{ paddingLeft: 10 }}>
                        <Text style={styles.title}>{this.state.name.charAt(0).toUpperCase() + this.state.name.slice(1)}</Text>
                    </Flex.Item>

                    {localStorage.getItem(`${this.props.appName}_objsToPush`) ?
                        <Flex.Item>
                            <Button
                                type='primary'
                                style={{ backgroundColor: this.props.colors && this.props.colors.secondary   }}
                                onPress={() => Sinc({ appName: this.props.appName, refreshScreen: () => this.refreshScreen() }) }
                                onClick={() => Sinc({ appName: this.props.appName, refreshScreen: () => this.refreshScreen() }) }
                                disabled={!navigator.onLine}
                            ><Text style={{ color: 'white' }}>Sincronizar</Text></Button>
                        </Flex.Item> : null
                    }
                </Flex>

                {this.state.info.searchOptions ?
                    <SearchBar
                        onSubmit={e => {
                            this.getList(1, this.state.info.searchOptions.fieldToSearch, 'like',  e)
                            this.setState({ searchValue: e, pageNumber: 1 })
                        }}
                        placeholder={<Text>Pesquisar</Text>}
                        cancelText={<Text>Cancelar</Text>}
                        style={{ fontSize: 18, backgroundColor: 'white', marginLeft: 20, marginRight: 20, borderRadius: 10 }}
                    /> : null
                }
                
                {list}
                {localList}
                
                <View style={{ marginTop: 10, marginLeft: 30 }}>
                    <ActivityIndicator
                        text={<Text>Carregando lista...</Text>}
                        animating={this.state.loadingList}
                        style={{ marginTop: 10, marginLeft: 30, marginBottom: 20 }}
                    />
                </View>

                {this.state.showMoreResultInfo ?
                    <View
                        onClick={() => this.getList(this.state.pageNumber, this.state.info.searchOptions.fieldToSearch, 'like',  this.state.searchValue)}
                        onPress={() => this.getList(this.state.pageNumber, this.state.info.searchOptions.fieldToSearch, 'like',  this.state.searchValue)}
                    >
                        <Text style={{ paddingTop: 20, paddingBottom: 20, textAlign: 'center', color: 'cornflowerblue' }}>Carregar mais registros</Text>
                    </View> : null
                }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    card: {
        // backgroundColor: this.props.colors && this.props.colors.secondary,
        backgroundColor: 'white',   
        borderRadius: 10,
        // height: 50,
        margin: 5,
        // height: 110,
        padding: 15,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 20,
        marginRight: 20,
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
    },
    container: {
        padding: 15,
        paddingTop: 25,
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 5,
        flex: 1,
    },
    title: {
        fontSize: 35,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    subtitle: {
        fontSize: 15,
        color: 'gray',
        marginBottom: 10,
    }
})

export default ListComponent