import React, { Component } from 'react'

// Modules
import { View, Text, ScrollView, Image } from 'react-native'
import { Container, Header, Left, Body, Right, Button, Icon, ListItem, Footer, FooterTab } from 'native-base'
// import { Drawer } from '@ant-design/react-native' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { Drawer } from 'antd-mobile' // PWA Projects: Utilize para projetos web (PWA)
import { LogOut } from '../utils/databaseFunctions'

// Components
import Form from './Form'
import List from './List'

// Styles
import 'antd-mobile/dist/antd-mobile.css' // PWA Projects: Utilize para projetos web (PWA)
import '../styles.css' // PWA Projects: Utilize para projetos web (PWA)

// Classe que cria as telas conforme os templates informados
export default class Screen extends Component {
    constructor(props){
        super(props)
        this.state = {
            templates: this.props.screenParams.templates || [],
            configs: this.props.screenParams.configs || {},
            menu: this.props.screenParams.menu || {},
            sidebarVisible: false,
            sidebarClosable: true,
            fildComponentState: {}
        }
    }

    componentDidMount = () => {
        setTimeout(() => {
            if(
                !(this.props.screenParams.accessOnlyLoggedUser && localStorage.getItem(this.state.configs['appName'])) &&
                (this.props.navigation.state.routeName !== 'Login' && this.props.navigation.state.routeName !== 'Signup' && this.props.navigation.state.routeName !== 'ForgotPass')
            ) this.props.navigation.push('Login')
            else if(this.props.screenParams.splash) this.props.navigation.push('Home')
        }, 2000)
    }

    /**
     * Usa o state dentro das funções dos componentes costumizados
     * @param {String} field Campo do state
     * @param {*} value Valor a ser colocado no campo
     */
    workWithState = (field, value) => {
        let fildComponentState = this.state.fildComponentState
        fildComponentState[field] = value
        this.setState({ fildComponentState })
    }

    render(){
        let sidebar = (
            <ScrollView style={{
                backgroundColor: 'white',
                paddingTop: 15,
                borderTopLeftRadius: 10,
                borderTopEndRadius: 10
            }}>
               {this.state.configs['headerMenuLinks'].map(item => {
                    return(
                        <ListItem
                            key={item.route} icon={item.icon || false}
                            onPress={() => {
                                if(this.props.navigation.state.routeName !== item.route)
                                    this.props.navigation.push(item.route)
                            }}
                        >
                            <Icon type="FontAwesome" active name={item.icon} style={{ width: 50, textAlign: 'center', fontSize: 18, color: 'gray'  }} />
                            <Text>{item.label}</Text>
                        </ListItem>
                    )
                })}

                {this.props.screenParams.accessOnlyLoggedUser ? 
                    <ListItem
                        key='logout' icon='sign-out'
                        onPress={() => {
                            LogOut({ appName: this.state.configs.appName })
                            .then(res => {
                                if(res.code) this.props.navigation.push('Login')
                                else alert(res.msg)
                            })
                        }}
                    >
                        <Icon type="FontAwesome" active name='sign-out' style={{ width: 50, textAlign: 'center', fontSize: 18, color: 'gray'  }} />
                        <Text>Sair</Text>
                    </ListItem>
                    : null
                }
    
            </ScrollView>
        )
        
        let screen = this.state.templates.map((template, index) => {
    
            // Verificar se o template é uma lista
            if(template.type === 'list')
                return <List key={index} template={template.info} name={template.name} colors={this.state.configs['colors']} navigation={this.props.navigation} appName={this.state.configs['appName']} filterByUsers={template.filterByUsers} />

            // Verificar se o template é um form
            if(template.type === 'form')
                return <Form key={index} template={template.fields} name={template.name} colors={this.state.configs['colors']} navigation={this.props.navigation} listRoute={template.listRoute} appName={this.state.configs['appName']} />

            // Verificar se o template é um custom
            if(typeof(template) === 'function')
                return template({...this.state.configs, navigation: this.props.navigation, state: this.state.fildComponentState, workWithState: (f, v) => this.workWithState(f, v)})
        })
        
        return(
            <Drawer
                sidebar={sidebar}
                position="bottom"
                open={this.state.sidebarVisible}
                drawerRef={el => (this.drawer = el)}
                onOpenChange={() => {
                    // PWA Projects: Utilize o bloco abaixo para projetos web (PWA)
                    if(this.state.sidebarClosable)
                        this.setState({ sidebarClosable: false })
                    else
                        this.setState({ sidebarVisible: false })
                }}
                contentStyle={{ top: 0 }}
            >
                <Container style={{ height: document.documentElement.clientHeight }}>
                    {this.state.menu.header ?
                        <Header style={{ backgroundColor: 'white', borderBottomColor: 'lightgray', borderBottomWidth: 1 }}>
                            <Left>
                                {
                                    this.props.navigation.state.routeName !== 'Home' ?
                                        <Icon 
                                            type="FontAwesome" style={{ fontSize: 16, color: 'gray' }} 
                                            name='chevron-left' 
                                            onClick={() => this.props.navigation.goBack()}
                                            onPress={() => this.props.navigation.goBack()}
                                        /> : null
                                }
                            </Left>
    
                            <Body>
                                {/* <Text style={{ color: 'black', fontSize: 18 }}>{this.state.configs['appName']}</Text>
                                <Text style={{ color: this.state.configs['colors'].primary, fontSize: 12 }}>{this.state.configs['shortDescription']}</Text> */}
                            </Body>
                            
                            <Right>
                                <Button transparent
                                    onPress={() => {
                                        this.drawer && this.drawer.openDrawer()
                                        this.setState({ sidebarVisible: true, sidebarClosable: true })
                                    } }
                                >
                                    <Icon type="FontAwesome" style={{ fontSize: 18, color: 'gray' }} name='bars' />
                                </Button>
                            </Right>
                        </Header> : null
                    }
    
                    <ScrollView style={{ backgroundColor: '#F5F5F5' }}>
                        <View>{screen}</View>
                    </ScrollView>

                    {this.state.menu.footer ?
                        <Footer style={{ backgroundColor: 'white', borderTopColor: 'lightgray', borderTopWidth: 1, height: 45 }}>
                            <FooterTab style={{ backgroundColor: 'white' }}>
                                {
                                    this.state.configs['footerMenuLinks'].map((item, i) => {
                                        return(
                                            <Button key={item.route} vertical
                                                onPress={() => {
                                                    if(this.props.navigation.state.routeName !== item.route)
                                                        this.props.navigation.push(item.route)
                                                }}
                                            >
                                                <Icon type="FontAwesome" name={item.icon} style={{ fontSize: 18, color: (this.props.navigation.state.routeName === item.route ? this.state.configs['colors'].primary : 'gray') }} />
                                                <Text style={{ fontSize: 'smaller', color: (this.props.navigation.state.routeName === item.route ? this.state.configs['colors'].primary : 'gray'), fontWeight: (this.props.navigation.state.routeName === item.route ? 'bold' : null) }}>{item.label}</Text>
                                            </Button>
                                        )
                                    })
                                }
                            </FooterTab>
                        </Footer> : null
                    }
                </Container>
            </Drawer>
        )
    }
}