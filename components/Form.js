import React, { Component } from 'react'

// Modules
import { AppLoading } from 'expo'
// import * as Font from 'expo-font' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { View, Text, StyleSheet } from 'react-native'
// import { Provider, InputItem, Picker, List, DatePicker, Checkbox, Button, Flex, Switch, TextareaItem, Modal, ActivityIndicator } from '@ant-design/react-native' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { InputItem, Picker, List, DatePicker, Checkbox, Button, Flex, Switch, TextareaItem, Modal, ActivityIndicator } from 'antd-mobile' // PWA Projects: Utilize para projetos web (PWA)
import enUs from 'antd-mobile/lib/date-picker/locale/en_US'
import { Create, Update, Read } from '../utils/databaseFunctions'
import { FormatDate } from '../utils/commonFunctions'

class FormComponent extends Component {

    constructor(props){
        super(props)
        this.state = {
            objectToEdit: this.props.navigation.state.params && this.props.navigation.state.params['objectToEdit'],
            colors: this.props.colors,
            form: {},
            name: this.props.name,
            fields: [],
            fieldsToRender: [],
            isReady: false,
            loadingButton: false,
            msg: null,
            requiredFields: []
        }
    }

    // Native Projects: Utilize para projetos nativos (IOS ou Android)
    // async componentDidMount() {
    //     await Font.loadAsync(
    //         'antoutline',
    //         // eslint-disable-next-line
    //         require('@ant-design/icons-react-native/fonts/antoutline.ttf')
    //     )
    
    //     await Font.loadAsync(
    //         'antfill',
    //         // eslint-disable-next-line
    //         require('@ant-design/icons-react-native/fonts/antfill.ttf')
    //     )
        
    //     let fields = this.props.template
        
    //     // Criar state com os campos dos form
    //     let form = {}
            // let requiredFields = []
    //     fields.forEach(element => {
            // Campos required
            // if(element.required) requiredFields.push(element.id)
            // if(this.state.objectToEdit){
            //     form[element.id] = this.state.objectToEdit[element.id] || ''
            //     fields = this.showHideDependentsFields(element.id, (this.state.objectToEdit[element.id] || ''), fields)
            // } else form[element.id] = null
    //     })
    //     if(this.state.objectToEdit) form['id'] = this.state.objectToEdit['id']
    //     // eslint-disable-next-line
    //     this.setState({ form, fields, isReady: true, requiredFields })
    // }

    // PWA Projects: Utilize para projetos web (PWA)
    componentDidMount = () => {
        let fields = this.props.template
        
        // Criar state com os campos dos form
        let form = {}
        let requiredFields = []
        fields.forEach(element => {
            // Campos required
            if(element.required) requiredFields.push(element.id)

            if(this.state.objectToEdit){
                form[element.id] = this.state.objectToEdit[element.id] || ''
                fields = this.showHideDependentsFields(element.id, (this.state.objectToEdit[element.id] || ''), fields)
            } else form[element.id] = null
        })
        if(this.state.objectToEdit) form['id'] = this.state.objectToEdit['id']
        this.setState({ form, fields, isReady: true, requiredFields })
    }

    // Exibir ou ocultar campos dependentes
    showHideDependentsFields = (field, value, fields = this.state.fields) => {
        fields.forEach(el => {
            if(el['id'] === field && el['showAnotherField']){
                el['showAnotherField'].forEach(el2 => {
                    if(el2.value === (typeof value === 'object' ? value[0] : value)){
                        fields = fields.map(el3 => {
                            if (el3['id'] === el2.field) el3['showField'] = true
                            return el3
                        })
                    } else {
                        fields = fields.map(el3 => {
                            if (el3['id'] === el2.field) el3['showField'] = false
                            return el3
                        })
                    }
                })
            }
        })
        return fields
    }

    // Edição de field
    setField = (field, value) => {
        
        // TODO: beforeSetValue - Função determinada no Template

        // Verificar se tem algum campo que deve ser exibido com a definição do campo atual
        let fields = this.showHideDependentsFields(field, value)

        let form = this.state.form
        form[field] = value
        this.setState({ form, fields })
    }

    // Enviar dados do form
    submit = () => {
        this.setState({ loadingButton: true })

        // TODO: beforeSubmit - Função determinada no Template

        // Verificar se os campos required foram preenchidos
        let filledForm = this.state.form
        let requiredFlag = true
        this.state.requiredFields.forEach(el => {
            if(!filledForm[el]) requiredFlag = false
        })

        if(requiredFlag){
            if(!this.state.objectToEdit){
                Create({
                    ref: this.state.name,
                    data: this.state.form,
                    appName: this.props.appName
                })
                .then(res => {
                    this.setState({
                        loadingButton: false,
                        msg: res.msg
                    })
    
                    // if(res.code) this.setState({ objectToEdit: { id: res.id } })
                    if(res.code) this.props.navigation.push(this.props.listRoute)
                })
            } else {
                Update({
                    ref: `${this.state.name}/${this.state.objectToEdit.id}`,
                    data: this.state.form,
                    appName: this.props.appName,
                    localIndex: this.state.objectToEdit.localIndex,
                    localRef: `${this.state.name}`,
                }).then(res => {
                    this.setState({
                        loadingButton: false,
                        msg: res.msg
                    })
                    this.props.navigation.push(this.props.listRoute)
                })
            }
        } else {
            Modal.alert(<Text style={{ fontSize: 18 }}>Erro</Text>, <Text style={{ color: '#8c8c8c' }}>Preencha todos os campos obrigatório</Text>, [{ text: <Text>Fechar</Text>, style: 'default'}])
            this.setState({ loadingButton: false })
        }
    }

    render(){
        // Construir formulário com base no template
        let fieldsToRender = this.state.fields.map((field, i) => {

            // Título
            if(field.type === 'title' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <View key={field.id} style={{ padding: 20, paddingTop: 30, paddingBottom: 10, backgroundColor: 'white' }}>
                        <Text style={styles.title} key={field.id}>
                            {field.value}
                        </Text>
                    </View>
                )
            }

            // Sub título
            else if(field.type === 'subtitle' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <View key={field.id} style={{ padding: 20, paddingTop: 0, paddingBottom: 10, backgroundColor: 'white' }}>
                        <Text key={field.id}
                            style={{
                                ...styles.subtitle,
                                color: 'gray'
                            }}
                        >
                            {field.value}
                        </Text>
                    </View>
                )
            }
            
            // Campo text
            else if(field.type === 'text' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <InputItem
                        key={field.id}
                        value={this.state.form[field.id]}
                        onChange={e => this.setField(field.id, e)}
                        placeholder={field.label}
                        style={{ fontSize: 15 }}
                    >
                        <Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text>
                    </InputItem>
                )
            }

            // Campo number
            else if(field.type === 'number' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <InputItem
                        key={field.id}
                        value={this.state.form[field.id]}
                        onChange={e => this.setField(field.id, e)}
                        placeholder={field.label}
                        keyboardType='numeric'
                        type='number'
                        style={{ fontSize: 15 }}
                    >
                        <Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text>
                    </InputItem>
                )
            }

            // Campo select
            else if(field.type === 'select' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <Picker
                        key={field.id}
                        placeholder={<Text>{field.label}</Text>}
                        data={field.options.map(opt => {
                            return { label: <Text>{opt.label}</Text> , value: opt.value }
                        })} cols={1}
                        okText={<Text>Ok</Text>} dismissText={<Text>Cancelar</Text>}
                        onOk={value => this.setField(field.id, value)}
                        title={<Text>{field.label}</Text>}
                        value={this.state.form[field.id] || [field.options[0].value]}
                    >
                        <List.Item arrow="horizontal"><Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text></List.Item>
                    </Picker>
                )
            }

            // Campo relation select
            else if(field.type === 'relation_select' && (field.showField || !field.waitAnotherFieldValue)){

                if(!field.options){
                    Read({ ref: field.option.class }).then(res => {
                        res = res.resultArray
                        let optRes = res.map(opt => { return {label: <Text>{opt[field.option.label]}</Text>, value: opt[field.option.value]} })
                        let fields = this.state.fields
                        fields = fields.map(child_field => {
                            if(child_field['id'] === field.id) child_field['options'] = optRes
                            return child_field
                        })
                        this.setState({ fields })
                    })
                }

                return(
                    <Picker
                        key={field.id}
                        placeholder={<Text>{field.label}</Text>}
                        data={field.options || [{label: <Text>Escolha</Text>, value: 'Escolha'}]} cols={1}
                        okText={<Text>Ok</Text>} dismissText={<Text>Cancelar</Text>}
                        onOk={value => this.setField(field.id, value)}
                        title={<Text>{field.label}</Text>}
                        value={this.state.form[field.id] || (field.options ? [field.options[0].value] : ['Escolha'])}
                    >
                        <List.Item arrow="horizontal">{<Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text>}</List.Item>
                    </Picker>
                )
            }

            // Campo date
            else if(field.type === 'date' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <DatePicker
                        key={field.id}
                        mode="date"
                        locale={enUs}
                        title={<Text>{field.label}</Text>}
                        extra={<Text>{field.label}</Text>}
                        okText={<Text>Ok</Text>} dismissText={<Text>Cancelar</Text>}
                        value={this.state.form[field.id] ? new Date(this.state.form[field.id]) : null}
                        onChange={value => this.setField(field.id, value)}
                        format={val => <Text>{`${FormatDate(val)}`}</Text>}
                        minDate={new Date(1950, 1, 1, 0, 0, 0)}
                        maxDate={new Date(2050, 12, 31, 23, 59, 59)}
                    >
                        <List.Item arrow="horizontal">{<Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text>}</List.Item>
                    </DatePicker>
                )
            }

            // Campo checkbox
            else if(field.type === 'checkbox' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <List key={field.id} renderHeader={<span style={{ color: 'black' }}><Text>{field.label}</Text></span>}>
                        {field.options.map((v, i) => (
                            <Checkbox.CheckboxItem
                                key={v.value}
                                checked={this.state.form[field.id] && this.state.form[field.id][i]}
                                onChange={e => {
                                    let localRes = this.state.form[field.id] || new Array(field.options.length)
                                    localRes[i] = e.target.checked
                                    this.setField(field.id, localRes)
                                }}
                            >
                                <Text>{v.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text>
                            </Checkbox.CheckboxItem>
                        ))}
                    </List>
                )
            }

            // Campo switch
            else if(field.type === 'switch' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <List key={field.id}>
                        <List.Item
                            extra={
                                <Switch
                                    checked={this.state.form[field.id]}
                                    onChange={e => this.setField(field.id, e)}
                                    checkboxStyle={{ color: this.state.colors && this.state.colors.primary }}
                                />
                            }
                        >
                            <Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text>
                        </List.Item>
                    </List>
                )
            }

            // Campo textarea
            else if(field.type === 'textarea' && (field.showField || !field.waitAnotherFieldValue)){
                return(
                    <List key={field.id} renderHeader={<span style={{ color: 'black' }}><Text>{field.label}<Text style={{ color: 'red' }}>{field.required ? '*' : ''}</Text></Text></span>}>
                        <TextareaItem
                            autoHeight
                            placeholder={field.label}
                            value={this.state.form[field.id]}
                            onChange={e => this.setField(field.id, e)}
                            style={{ fontSize: 15 }}
                        />
                    </List>
                )
            }
            
        })

        if (!this.state.isReady) {
            return <AppLoading />;
        }

        return(
            // Native Projects: Utilize a tag Provider para projetos nativos (IOS ou Android), para web (PWA) utilize a tag View
            <View style={{ backgroundColor: 'white' }}>
                <Flex.Item style={{ paddingLeft: 30, paddingTop: 10 }}>
                    <Text style={{fontSize: 35, fontWeight: 'bold', marginBottom: 5}}>{this.state.name.charAt(0).toUpperCase() + this.state.name.slice(1)}</Text>
                </Flex.Item>

                {fieldsToRender}

                <Flex style={{ padding: 20 }}>
                    <Flex.Item>
                        <Button
                            type='primary'
                            style={{
                                backgroundColor: this.state.colors && this.state.colors.primary,
                            }}
                            full
                            // loading={this.state.loadingButton}
                            onClick={() => this.submit()}
                            onPress={() => this.submit()}
                        ><Text style={{ color: 'white' }}>Salvar</Text></Button>
                    </Flex.Item>
                </Flex>

                <Text
                    style={{
                        textAlign: 'center',
                        marginTop: 20,
                        // fontSize: 'small'
                    }}
                >{this.state.msg}</Text>

                <ActivityIndicator
                    toast
                    text={<Text style={{ color: 'white' }}>Carregando...</Text>}
                    animating={this.state.loadingButton ? this.state.loadingButton : false}
                />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        paddingTop: 25,
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 5,
        flex: 1,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: 15,
        color: 'gray',
    }
})

export default FormComponent