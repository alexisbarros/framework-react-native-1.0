const template_form = {
    name: 'Template',
    type: 'list',
    info: {
        title: 'field_1',
        subtitle: 'field_2',
        extra: 'field_7',
        image: null,
        routeToUpdateData: 'Form',
        searchOptions: {
            fieldToSearch: 'field_1'
        }
    },
    filterByUsers: ['owner'],
}

export default template_form