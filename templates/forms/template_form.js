const template_form = {
    name: 'Template',
    type: 'form',
    fields: [
        {
            id: 'field_title',
            type: 'title',
            value: 'Title'
        },
        {
            id: 'field_subtitle',
            type: 'subtitle',
            value: 'SubTitle'
        },
        {
            id: 'field_1',
            label: 'Text',
            type: 'text',
            required: true,
        },
        {
            id: 'field_2',
            label: 'Number',
            type: 'number',
            step: 0.1,
            required: true,
        },
        {
            id: 'field_3',
            label: 'Select',
            type: 'select',
            options: [
                {label: 'Option 1' , value: 'option_1'},
                {label: 'Option 2' , value: 'option_2'},
                {label: 'Option 3' , value: 'option_3'},
            ],
            // Exibir um ou vários campos dependendo do valor informado neste campo
            showAnotherField: [
                { field: 'field_4', value: 'option_2' }
            ]
        },
        {
            id: 'field_4',
            label: 'Date',
            type: 'date',
            // Esconder campo para aguardar outro campo o exibir
            waitAnotherFieldValue: true
        },
        {
            id: 'field_5',
            label: 'Relation Select',
            type: 'relation_select',
            option: {
                class: 'Template',
                label: 'field_1',
                value: 'field_1'
            }
        },
        {
            id: 'field_6',
            label: 'Switch',
            type: 'switch',
        },
        {
            id: 'field_7',
            label: 'TextArea',
            type: 'textarea'
        },
        {
            id: 'field_8',
            label: 'Checkbox',
            type: 'checkbox',
            options: [
                {label: 'Option 1' , value: 'option_1'},
                {label: 'Option 2' , value: 'option_2'},
                {label: 'Option 3' , value: 'option_3'},
            ],
        },
    ],
    listRoute: 'List'
}

export default template_form