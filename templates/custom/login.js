import React from 'react'

// Modules
import { View, Text } from 'react-native'
// import { InputItem, Button, ActivityIndicator, Accordion, List, Modal } from '@ant-design/react-native' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { InputItem, Button, ActivityIndicator, Accordion, List, Modal } from 'antd-mobile' // PWA Projects: Utilize para projetos web (PWA)
import { LogIn, LocalLogIn } from '../../utils/databaseFunctions'

function login (props) {

    let email = ''
    let password = ''

    let setField = (field, value) => {
        if(field === 'email') email = value
        else if(field === 'pass') password = value
    }

    let submit = () => {
        props.workWithState('login_loading', true)

        if(props.localUsers){
            let res = LocalLogIn({ email, password, appName: props.appName, localUsers: props.localUsers })
            
            if(res.code) props.navigation.push('Home')
            else Modal.alert(<Text style={{ fontSize: 18 }}>Erro</Text>, <Text style={{ color: '#8c8c8c' }}>{res.msg}</Text>, [{ text: <Text>Fechar</Text>, style: 'default'}])

            props.workWithState('login_loading', false)
        } else {
            LogIn({ email, password, appName: props.appName  })
            .then(res => {
                if(res.code) props.navigation.push('Home')
                else Modal.alert(<Text style={{ fontSize: 18 }}>Erro</Text>, <Text style={{ color: '#8c8c8c' }}>{res.msg}</Text>, [{ text: <Text>Fechar</Text>, style: 'default'}])

                props.workWithState('login_loading', false)
            })
        }
    }

    return(
        <View className='card'>
            <View style={{ padding: 15, padding: 40 }}>
                <Text style={{ fontSize: 35, fontWeight: 'bold' }}>Login</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#8c8c8c' }}>
                    Olá! seja bem vindo.
                </Text>
            </View>

            <View style={{ marginBottom: 10, paddingTop: 10 }}>
                <InputItem
                    key='email'
                    onChange={e => setField('email', e)}
                    placeholder='E-mail'
                    type='email-address'
                    clear
                />
            
                <InputItem
                    key='pass'
                    onChange={e => setField('pass', e)}
                    placeholder='Senha'
                    type='password'
                    clear
                />
            </View>

            <View style={{ padding: 20, paddingTop: 0 }}>

                <Button
                    type='primary'
                    style={{
                        marginTop: 30, 
                        marginBottom: 30,
                        backgroundColor: props.colors && props.colors.primary,
                        width: '100%',
                        borderRadius: 15
                    }}
                    full
                    onClick={() => submit()}
                    onPress={() => submit()}
                ><Text style={{ color: 'white' }}>Entrar</Text></Button>

                <Accordion>
                    <Accordion.Panel>
                        <List>
                            <List.Item 
                                onClick={() => props.navigation.push('Signup')}
                                onPress={() => props.navigation.push('Signup')}
                            >
                                <Text style={{ color: props.colors && props.colors.secondary }}>Registre-se</Text>
                            </List.Item>

                            <List.Item 
                                onClick={() => props.navigation.push('ForgotPass')}
                                onPress={() => props.navigation.push('ForgotPass')}
                            >
                                <Text style={{ color: props.colors && props.colors.secondary }}>Esqueci a senha</Text>
                            </List.Item>
                        </List>
                    </Accordion.Panel>
                </Accordion>
            </View>

            <ActivityIndicator
                toast
                text={<Text style={{ color: 'white' }}>Carregando...</Text>}
                animating={props.state.login_loading ? props.state.login_loading : false}
            />
        </View>
    )
}


export default login