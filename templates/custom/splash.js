import React from 'react'

// Modules
import { View, Image } from 'react-native'

let splash_screen = (props) => {

    return(
        <View style={{
            height: document.documentElement.clientHeight, 
            backgroundColor: props.colors && props.colors.primary,
            flex:1,justifyContent: "center",alignItems: "center"
        }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../../assets/logo.png')} style={{ height: 110, width: 110 }} />
            </View>
        </View>
    )
}

export default splash_screen