import React from 'react'

// Modules
import { View, Text, StyleSheet } from 'react-native'
// import { InputItem, Button, Accordion, List, Modal, ActivityIndicator } from '@ant-design/react-native' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { InputItem, Button, Accordion, List, Modal, ActivityIndicator } from 'antd-mobile' // PWA Projects: Utilize para projetos web (PWA)
import { ResetPass } from '../../utils/databaseFunctions'

let forgotPass = (props) => {
    const styles = StyleSheet.create({
        container: {
            padding: 15,
            paddingTop: 30,
            paddingBottom: 30,
            margin: 15,
            marginTop: 50,
            backgroundColor: 'white',
            borderRadius: 5,
            flex: 1,
            // justifyContent: 'center',
            // alignItems: 'center'
        }
    })

    let email = ''

    let submit = () => {
        props.workWithState('forgot_pass_loading', true)

        ResetPass({ email  })
        .then(res => {
            Modal.alert(<Text style={{ fontSize: 18 }}>Erro</Text>, <Text style={{ color: '#8c8c8c' }}>{res.msg}</Text>, [{ text: <Text>Fechar</Text>, style: 'default'}])
            props.workWithState('forgot_pass_loading', false)
        })
    }

    return(
        <View className='card'>
            <View style={{ padding: 15, padding: 40 }}>
                <Text style={{ fontSize: 35, fontWeight: 'bold' }}>Recupere sua senha!</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#8c8c8c' }}>
                    Informe seu e-mail.
                </Text>
            </View>

            <View style={{ paddingBottom: 0, paddingTop: 10 }}>
                <InputItem
                    key='email'
                    onChange={e => email = e}
                    placeholder='E-mail'
                    type='email-address'
                    clear
                />
            </View>

            <View style={{ padding: 20, paddingTop: 0 }}>
                <Button
                    type='primary'
                    style={{
                        marginTop: 30, 
                        marginBottom: 30,
                        backgroundColor: props.colors && props.colors.primary,
                        width: '100%',
                        borderRadius: 15
                    }}
                    full
                    onClick={() => submit()}
                    onPress={() => submit()}
                ><Text style={{ color: 'white' }}>Recuperar senha</Text></Button>

                <Accordion>
                    <Accordion.Panel>
                        <List>
                            <List.Item 
                                onClick={() => props.navigation.push('Login')}
                                onPress={() => props.navigation.push('Login')}
                            >
                                <Text style={{ color: props.colors && props.colors.secondary }}>Login</Text>
                            </List.Item>
                        </List>
                    </Accordion.Panel>
                </Accordion>
            </View>

            <ActivityIndicator
                toast
                text={<Text style={{ color: 'white' }}>Carregando...</Text>}
                animating={props.state.forgot_pass_loading ? props.state.forgot_pass_loading : false}
            />
        </View>
    )
}


export default forgotPass