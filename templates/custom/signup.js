import React from 'react'

// Modules
import { View, Text, StyleSheet } from 'react-native'
// import { InputItem, Button, Accordion, List, Modal, ActivityIndicator } from '@ant-design/react-native' // Native Projects: Utilize para projetos nativos (IOS ou Android)
import { InputItem, Button, Accordion, List, Modal, ActivityIndicator } from 'antd-mobile' // PWA Projects: Utilize para projetos web (PWA)
import { SignIn } from '../../utils/databaseFunctions'

let signup = (props) => {
    const styles = StyleSheet.create({
        container: {
            padding: 15,
            paddingTop: 30,
            paddingBottom: 30,
            margin: 15,
            marginTop: 50,
            backgroundColor: 'white',
            borderRadius: 5,
            flex: 1,
            // justifyContent: 'center',
            // alignItems: 'center'
        }
    })

    let email = ''
    let password = ''
    let repeatPass = ''

    let setField = (field, value) => {
        if(field === 'email') email = value
        else if(field === 'pass') password = value
        else if(field === 'repeatPass') repeatPass = value
    }

    let submit = () => {
        if(password !== repeatPass) alert('As senhas digitadas não são iguais')
        else {
            props.workWithState('signup_loading', true)

            SignIn({ email, password, appName:props.appName  })
            .then(res => {
                if(res.code) props.navigation.push('Home')
                else Modal.alert(<Text style={{ fontSize: 18 }}>Erro</Text>, <Text style={{ color: '#8c8c8c' }}>{res.msg}</Text>, [{ text: <Text>Fechar</Text>, style: 'default'}])
                props.workWithState('signup_loading', false)
            })
        }
    }

    return(
        <View className='card'>
            <View style={{ padding: 15, padding: 40 }}>
                <Text style={{ fontSize: 35, fontWeight: 'bold' }}>Registre-se!</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#8c8c8c' }}>
                    Crie aqui sua conta.
                </Text>
            </View>

            <View style={{ paddingBottom: 0, paddingTop: 10 }}>
                <InputItem
                    key='email'
                    onChange={e => setField('email', e)}
                    placeholder='E-mail'
                    type='email-address'
                    clear
                />

                <InputItem
                    key='pass'
                    onChange={e => setField('pass', e)}
                    placeholder='Senha'
                    type='password'
                    clear
                />

                <InputItem
                    key='repeat-pass'
                    onChange={e => setField('repeatPass', e)}
                    placeholder='Repita a Senha'
                    type='password'
                    clear
                />
            </View>

            <View style={{ padding: 20, paddingTop: 0 }}>
                <Button
                    type='primary'
                    style={{
                        marginTop: 30,
                        marginBottom: 30,
                        backgroundColor: props.colors && props.colors.primary,
                        width: '100%',
                        borderRadius: 15
                    }}
                    full
                    onClick={() => submit()}
                    onPress={() => submit()}
                ><Text style={{ color: 'white' }}>Registrar</Text></Button>
                
                <Accordion>
                    <Accordion.Panel>
                        <List>
                            <List.Item 
                                onClick={() => props.navigation.push('Login')}
                                onPress={() => props.navigation.push('Login')}
                            >
                                <Text style={{ color: props.colors && props.colors.secondary }}>Login</Text>
                            </List.Item>
                        </List>
                    </Accordion.Panel>
                </Accordion>
            </View>
            
            <ActivityIndicator
                toast
                text={<Text style={{ color: 'white' }}>Carregando...</Text>}
                animating={props.state.signup_loading ? props.state.signup_loading : false}
            />
        </View>
    )
}


export default signup