const functions = require('firebase-functions')
const cors = require('cors')({origin: true})

// The Firebase Admin SDK to access Database.
const admin = require('firebase-admin')
admin.initializeApp()

/**
 * Retorna os registros filtrado pelos parametros informados.
 * @param {Object} Parametros: ref, fieldToSearch, operator ('equal', 'like'), value, pageNumber.
 * @returns Array de resultado.
 */
exports.read = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        let resultArray = []
        let countResult = 0
        let props = JSON.parse(request.body)
        admin.database().ref(props.ref).once('value').then(res => {
            res.forEach(el => {
                resultArray.push(el.val())
            })

            if(props.operator === 'equal'){
                resultArray = resultArray.filter(snap => { return snap[props.fieldToSearch] === props.value })
            } else if (props.operator === 'like'){
                resultArray = resultArray.filter(snap => { return snap[props.fieldToSearch].toLowerCase().includes(props.value.toLowerCase()) })
            }

            countResult = resultArray.length
            if(props.pageNumber) resultArray = resultArray.slice(0, (props.pageNumber * 10))

            return response.send({resultArray, countResult})
        })
        .catch(err => {
            return response.send(err.message)
        })
    
    })
})
